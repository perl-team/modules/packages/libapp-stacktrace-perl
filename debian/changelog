libapp-stacktrace-perl (0.09-5) unstable; urgency=medium

  * Team upload.
  * Add patch from Niko Tyni for Perl 5.38 compatibility.
    (Closes: #1051609)
  * Refresh lintian override (format).
  * Declare compliance with Debian Policy 4.6.2.
  * Update upstream metadata fields: Repository, Repository-Browse.

 -- gregor herrmann <gregoa@debian.org>  Sun, 10 Sep 2023 22:45:10 +0200

libapp-stacktrace-perl (0.09-4) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.
  * Update 'DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow' to '=+all'.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 08 Jun 2022 17:15:05 +0100

libapp-stacktrace-perl (0.09-3) unstable; urgency=medium

  * Declare compliance with Debian Policy 4.1.1.
  * Set "Rules-Requires-Root: no".

 -- Axel Beckert <abe@debian.org>  Mon, 20 Nov 2017 06:07:00 +0100

libapp-stacktrace-perl (0.09-2) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.
  * debian/upstream/metadata: use HTTPS for GitHub URLs.

  [ Axel Beckert ]
  * Add patch by Niko Tyni to fix FTBFS with newer toolchain. Thanks!
    (Closes: #775744)
  * Enable bindnow build hardening flag.
  * Declare compliance with Debian Policy 3.9.8.
  * Add perl-debug as (build-)dependency. Thanks Niko Tyni!
    (Also closes: #775744)
  * Switch debhelper compatibility to 10.

 -- Axel Beckert <abe@debian.org>  Sat, 10 Dec 2016 23:18:45 +0100

libapp-stacktrace-perl (0.09-1) unstable; urgency=low

  * Initial Release. (Closes: #772966)

 -- Axel Beckert <abe@debian.org>  Fri, 12 Dec 2014 17:43:53 +0100
